using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARControl : MonoBehaviour
{
    private ARRaycastManager raycastManager;
    private ARPointCloudManager pointCloudManager;
    private ARPlaneManager planeManager;

    private List<ARRaycastHit> hits = new List<ARRaycastHit>();

    [SerializeField] GameObject atomPrefab;
    [SerializeField] Material atomMaterial;
    [SerializeField] Material atomActivatedMaterial;

    [SerializeField] Material electronMaterial;
    [SerializeField] Material electronActivatedMaterial;

    [SerializeField] GameObject switcherPrefab;
    [SerializeField] Material switcherGreenMaterial;
    [SerializeField] Material switcherRedMaterial;

    Camera arCam;

    private bool lightOn;
    private bool atomPlaced;
    private bool switcherPlaced;

    private GameObject atomObject;
    private GameObject switcherObject;

    public float rotationVelocity;

    // Start is called before the first frame update
    void Start()
    {
        raycastManager = GetComponent<ARRaycastManager>();
        pointCloudManager = GetComponent<ARPointCloudManager>();
        planeManager = GetComponent<ARPlaneManager>();

        arCam = GameObject.Find("AR Camera").GetComponent<Camera>();

        lightOn = false;
        atomPlaced = false;
        switcherPlaced = false;

        atomObject = null;
        switcherObject = null;

        planeManager.enabled = false;
        foreach (var Plane in planeManager.trackables)
        {
            Plane.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 0 || Input.GetTouch(0).phase != TouchPhase.Began)
        {
            return;
        }

        if (!atomPlaced)
        {
            if (raycastManager.Raycast(Input.GetTouch(0).position, hits, TrackableType.All))
            {
                atomObject = Instantiate(atomPrefab, hits[0].pose.position, hits[0].pose.rotation);
                atomPlaced = true;

                pointCloudManager.enabled = false;
                foreach (var Point in pointCloudManager.trackables)
                {
                    Point.gameObject.SetActive(false);
                }

                planeManager.enabled = true;
            }

            return;
        }

        if (!switcherPlaced)
        {
            if (raycastManager.Raycast(Input.GetTouch(0).position, hits, TrackableType.Planes))
            {
                switcherObject = Instantiate(switcherPrefab, hits[0].pose.position, hits[0].pose.rotation);
                switcherPlaced = true;

                planeManager.enabled = false;
                foreach (var Plane in planeManager.trackables)
                {
                    Plane.gameObject.SetActive(false);
                }
            }

            return;
        }

        if (atomPlaced && switcherPlaced)
        {
            RaycastHit hit;
            Ray ray = arCam.ScreenPointToRay(Input.GetTouch(0).position);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.tag == "Switcher")
                {
                    if (!lightOn)
                    {
                        atomObject.GetComponent<MeshRenderer>().material = atomActivatedMaterial;
                        atomObject.GetComponent<Light>().enabled = true;

                        atomObject.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material = electronActivatedMaterial;
                        atomObject.transform.GetChild(0).gameObject.GetComponent<Light>().enabled = true;

                        atomObject.transform.GetChild(1).gameObject.GetComponent<MeshRenderer>().material = electronActivatedMaterial;
                        atomObject.transform.GetChild(1).gameObject.GetComponent<Light>().enabled = true;

                        switcherObject.GetComponent<MeshRenderer>().material = switcherGreenMaterial;
                    }
                    else
                    {
                        atomObject.GetComponent<MeshRenderer>().material = atomMaterial;
                        atomObject.GetComponent<Light>().enabled = false;

                        atomObject.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material = electronMaterial;
                        atomObject.transform.GetChild(0).gameObject.GetComponent<Light>().enabled = false;

                        atomObject.transform.GetChild(1).gameObject.GetComponent<MeshRenderer>().material = electronMaterial;
                        atomObject.transform.GetChild(1).gameObject.GetComponent<Light>().enabled = false;

                        switcherObject.GetComponent<MeshRenderer>().material = switcherRedMaterial;
                    }
                    lightOn = !lightOn;
                }
            }
        }
    }

    void FixedUpdate()
    {
        if (!lightOn)
        {
            return;
        }
        else
        {
            var atomPosition = atomObject.transform.position;
            var x = atomPosition.x;
            var y = atomPosition.y;
            var z = atomPosition.z;

            var electron1Position = atomObject.transform.GetChild(0).gameObject.transform.position;
            var x1 = electron1Position.x;
            var y1 = electron1Position.y;
            var z1 = electron1Position.z;

            var angle = rotationVelocity * Time.deltaTime;

            var newX1 = x + (x1 - x) * (float) Math.Cos(angle) - (z1 - z) * (float) Math.Sin(angle);
            var newZ1 = z + (z1 - z) * (float)Math.Cos(angle) + (x1 - x) * (float)Math.Sin(angle);

            atomObject.transform.GetChild(0).gameObject.transform.position = new Vector3(newX1, y1, newZ1);

            var electron2Position = atomObject.transform.GetChild(1).gameObject.transform.position;
            var x2 = electron2Position.x;
            var y2 = electron2Position.y;
            var z2 = electron2Position.z;

            var newY2 = y + (y2 - y) * (float)Math.Cos(angle) - (z2 - z) * (float)Math.Sin(angle);
            var newZ2 = z + (z2 - z) * (float)Math.Cos(angle) + (y2 - y) * (float)Math.Sin(angle);

            atomObject.transform.GetChild(1).gameObject.transform.position = new Vector3(x2, newY2, newZ2);

        }
        return;
    }
}
